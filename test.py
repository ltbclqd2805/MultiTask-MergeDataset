import tensorflow as tf
import numpy as np

a = tf.placeholder(tf.float32, [None, 15])
vector = tf.constant(0., tf.float32, [15])

equal = tf.cast(tf.equal(a, vector), tf.float32)
split = tf.slice(a, [0, 5], [3, 5])

sess = tf.InteractiveSession()

v = np.zeros([3, 15], np.float32)
for j in range(3):
    for i in range(15):
        if i < 5:
            v[j][i] = 0.0
        else:
            if i < 10:
                v[j][i] = 1.0
            else:
                v[j][i] = 2.0

sess.run(tf.global_variables_initializer())
e, s = sess.run([equal, split], feed_dict={a: v})
print(e)
print(s)
